import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="ttvlib",
    version="0.3",
    author="TTV",
    author_email="trektv@gmail.com",
    description="A package with useful dependencies for TTV.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://trektvpodcast@bitbucket.org/trek-tv-podcast/ttv-lib.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: Apache License Version 2.0",
        "Operating System :: OS Independent",
    ],
)
