# README #

TTV Lib

### What is this repository for? ###

* Trek TV public dependencies library.

### Notes
* git repo is TTV account bitbucket.

### Dev environment

Create a virtual environment

```
virtualenv ttvlibvenv
```

Activate your virtual environment

```
.\ttvlibvenv\Scripts\activate
```