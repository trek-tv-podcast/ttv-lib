import json


class TTVBe:
    def __init__(self):
        pass

    def return_response(self, code=500, data={}, methods='POST, GET, OPTIONS',
                        headers='*'):
        headers = {
            'ContentType': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': headers,
            'Access-Control-Allow-Methods': methods
        }

        response_data = json.dumps(data)

        return response_data, code, headers
